-- create database exercicio;
-- use exercicio;

create table paciente (
codigo int not null,
nome varchar(80) not null,
cpf int,
sexo char(1),
codClinica int,
check (sexo in ('M','F'))
);

create table clinica(
codigo int not null,
nome varchar(80) not null,
local varchar(100),
primary key (codigo)
);
-- adicionando o campo data na tabela paciente:
alter table paciente add column data_ingresso date;
-- alterando o campo cpf na tabela paciente:
alter table paciente modify cpf char(11);
-- adicionando restrição de chave primária na tabela paciente:
alter table paciente add primary key (codigo);
-- adicionando restrição de chave estrangeira na tabela paciente:
alter table paciente add constraint chaveEsPac foreign key (codClinica) references paciente (codigo);
-- renomear o nome de alguma coluna:
alter table paciente change data_ingresso dataNascimento date;
-- apagar coluna da tabela:
alter table paciente drop column dataNascimento;
-- renomear a tabela:
rename table paciente to pacienteHospital;
-- adicionar coluna e seu valor padrão:
alter table pacientehospital add column planoSaude varchar(40) default "GEAP";
-- adicionando restriçao de padrões para a coluna planoSaude:
alter table pacientehospital add check (planoSaude in ('GEAP', 'UNIMED', 'SUL-ÁMERICA'));
-- excluindo a table pacienteHospital:
