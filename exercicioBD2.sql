-- create database aula2_p3;
-- use aula2_p3;

create table departamento (
codigo char(2),
nome varchar(50),
primary key (codigo)
);

create table funcionario (
codigofunc int,
nome varchar(50) not null,
cpf char(12) not null,
salario decimal(5,2),
coddep char(2),
primary key (codigofunc),
foreign key (coddep) references departamento (codigo)
);

-- comando de seleção de dados:
select * from departamento;
select * from funcionario;

-- comando para inserir nas tabelas:
insert into departamento (codigo, nome) values ('A1','Computação');
insert into departamento (codigo, nome) values ('B1','Direito');
-- se souber a ordem que esta os atributos na tablea ja pode inserir dessa forma:
insert into departamento values ('C1', 'Medicina');

-- inserir dados na tabela paciente:
insert into funcionario values (1,'João','0998779-02',100.04,'A1');
insert into funcionario values (2,'Pedro','9997776-00',200.83,'B2');
insert into funcionario values (3,'Maria','8887774-66',180.54,'C3');

-- para atualizar valores dentro da tabela:
update funcionario set salario = 300.77 where codigofunc = 1;

-- para deletar valores nas tabelas: igual a 0 desabilita, igual a 1 habilita.
delete from departamento where nome = 'Direito';

-- para desativar o modo de deleção seguro:  igual a 0 desabilita, igual a 1 habilita.
set sql_safe_updates = 0;

-- alterar ou adicionar valor na tabela:
alter table funcionario add primary key (codigo);

-- alterar ou adicionar uma nova coluna na tabela:
alter table funcionario add column categoria varchar(25);

-- deletar uma coluna:
alter table funcionario drop column salario;

-- modificar o tipo de dado de uma coluna:
alter table funcionario modify categoria char(1);

-- alterar no nome de uma coluna:
alter table funcionario change nome full_name varchar(50);

-- renomear tabela:
rename table funcionario to colaborador;

-- descrever a tabela:
describe funcionario;