-- create database Aula1_p3;
-- use Aula1_p3;

-- criando a tabela paciente;

create table paciente (
-- "not null" quer dizer que o campo destacado com ele, não pode estar vazio.
codigo int not null,
nome varchar(50) not null,
cpf char(12) not null,
data_nascimento date,
sexo char(1),
planoSaude varchar(30),
unique (cpf),
primary key (codigo),
check (sexo in ('M', 'F'))
);

create table medico (
crm varchar(15) not null,
nome varchar(50) not null,
sexo char(1),
especialidade varchar (25) default 'Clinico',
primary key (crm),
check  (sexo in ('M', 'F')),
check (especialidade in ('Cardiologista', 'Geriatra', 'Clinico'))
);

create table consulta (
codmedico varchar(15) not null,
codpaciente int not null,
-- valor decimal quer dizer que o valor pode assumir até 7 algarismos e 2 deles vão estar na casa decimal.
valor decimal (7,2),
data_consulta date,
primary key (codmedico, codpaciente),
-- "constraint" é pra dar um nome a chave estrangeira.
constraint chavemedico foreign key (codmedico) references medico(crm),
constraint chavepaciente foreign key (codpaciente) references paciente(codigo)
);