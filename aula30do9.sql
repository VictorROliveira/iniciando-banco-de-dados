create database livros;
use livros;

create table editora (
codigo int not null,
nome varchar(50),
primary key (codigo)
);

create table assunto (
sigla char(2),
nome varchar(50),
primary key (sigla)
);

-- drop table assunto;
-- drop table livro; 

create table livro (
codigo int not null,
titulo varchar(50),
valor decimal(3,2),
ano varchar(4),
assunto char(2),
editora int not null,

constraint fk_ass foreign key (assunto) references assunto (sigla),
constraint fb_codigo foreign key (editora) references editora (codigo)
);

insert into editora (codigo, nome) values (1,'Pearson');
insert into editora (codigo, nome) values (2,'Saraiva');
insert into editora (codigo, nome) values (3,'Bookman');
insert into editora (codigo, nome) values (4,'Unipê');

insert into assunto (sigla, nome) values ('BD','Banco de Dados');
insert into assunto (sigla, nome) values ('GP','Gerência de Projetos');
insert into assunto (sigla, nome) values ('RC','Redes de Computadores');
insert into assunto (sigla, nome) values ('ES','Engenharia de Software');
insert into assunto (sigla, nome) values ('PR','Programação');

alter table livro modify valor decimal(7,2);

insert into livro (codigo, titulo, valor, ano, assunto, editora) values (1,'MySQL',100.00,'2014','BD',1);
insert into livro (codigo, titulo, valor, ano, assunto, editora) values (2,'Introdução ao PMBOK',88.00,'2014','GP',2);
insert into livro (codigo, titulo, valor, ano, assunto, editora) values (3,'JAVA',75.00,'2014','PR',2);
insert into livro (codigo, titulo, valor, ano, assunto, editora) values (4,'Engenharia de Software',120.00,'2013','ES',3);
insert into livro (codigo, titulo, valor, ano, assunto, editora) values (5,'Python',65.00,'2015','PR',4);
insert into livro (codigo, titulo, valor, ano, assunto, editora) values (6,'Oracle',150.00,'2016','BD',4);
insert into livro (codigo, titulo, valor, ano, assunto, editora) values (7,'Redes de Computadores',99.00,'2016','RC',1);

-- pegar valores e separar em um grupo:
select assunto from livro group by assunto;
-- selecionar o livro mais caro:
select assunto, max(valor) from livro group by assunto;
-- renomear pra definir qual o livro mais caro:
select assunto, max(valor) as 'Livro mais caro:' from livro group by assunto;
-- ordenar os maiores valores:
select assunto, max(valor) as 'Livro mais caro:' from livro group by assunto order by assunto;
-- contar quantos registros existem:[
select editora, count(*) from livro group by editora;
-- retornar soma de valores:
select assunto, sum(valor) as 'Soma dos Valores é:' from livro group by assunto;
-- retornar valores onde a soma é maior que um valor determinado:
select assunto, sum(valor) as 'Soma dos Valores é:' from livro group by assunto having sum(valor) > 140;
-- verificar em mais de uma tabela:
select livro.titulo, livro.valor, editora.nome from livro, editora where livro.editora=editora.codigo;
-- checar duas ou mais tabelas:
select livro.titulo, livro.valor, editora.nome from livro, editora where livro.editora=editora.codigo;
-- checar duas ou mais tabelas(outra forma):
select l.titulo, l.valor, l.ano, a.nome from livro l, assunto a where l.assunto=a.sigla;
-- selecionando nome de livros da tabela em que o livro começa com a letra B:
select nome from assunto where nome like 'B%';
-- selecionando nome de livros da tabela em que o livro termina com determinada letra:
select nome from assunto where nome like '%e';
-- selecionando nome de livros da tabela em que o livro possui a letra(ou letras) em qualquer posição:
select nome from assunto where nome like '%de%';
-- selecionar valores da tabela sem que haja repetição:
select distinct sigla from assunto;
-- buscando em tableas na ordem crescente e decrescente:
select titulo, valor from livro order by titulo asc, valor desc;
-- selecionando determinada coisas exceto tal coisa:
select * from assunto where sigla not between 'BD' and 'GP';
-- para calcular media aritmetica de valores:
select codigo, avg(valor) from livro;
-- selecionar colunas da tabela + aumento de 30% no valor dos livros:
select codigo, titulo, valor*1.3 from livro;

