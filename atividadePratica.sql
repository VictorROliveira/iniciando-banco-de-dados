create database sistema_bancario;
use sistema_bancario;

create table banco (
codigo int not null,
nome varchar(50) not null,
primary key (codigo)
);

create table agencia (
cod_banco int not null,
numero_agencia varchar(15),
primary key (numero_agencia),
constraint chaveESBan foreign key (cod_banco) references banco (codigo)
);

create table cliente (
cpf varchar(12) not null,
nome varchar(50) not null,
sexo char(1),
endereco varchar(30),
check (sexo in ('M', 'F')),
primary key (cpf)
);

create table conta (
numero_conta varchar(8) not null,
saldo decimal(7,2),
tipo_conta int,
num_agenda varchar(10),
primary key (numero_conta)
);

create table historico (
cpf_cliente varchar(12) not null,
num_conta varchar(8) not null,
data_inicio date,
constraint chaveESsec foreign key (cpf_cliente) references cliente (cpf)
);

create table telefone_cliente (
cpf_cli varchar(12) not null,
telefone varchar(12),
constraint chaveESter foreign key (cpf_cli) references cliente (cpf)
);

alter table agencia add column endereco varchar(100);

-- inserindo dados nas colunas da tabela banco:
insert into banco (codigo,nome) values (1,'Banco do Brasil');
insert into banco (codigo,nome) values (5,'CEF');

-- inserindo dados nas colunas da tabelas agência:
insert into agencia (cod_banco,numero_agencia,endereco) values (4,'322','Av.Walfredo Macedo Brandao, 1139');
insert into agencia (cod_banco,numero_agencia,endereco) values (1,'1253','R.Bancário Sérgio Guerra, 17');

-- inserindo dados nas colunas da tabela cliente:
insert into cliente (cpf,nome,sexo,endereco) values ('111222333-44','Bruna Andrade','F','Rua José Firmino Ferreira,1050');
insert into cliente (cpf,nome,sexo,endereco) values ('666777888-99','Radegondes Silva','M','Av.Epitácio Pessoa,1008');
insert into cliente (cpf,nome,sexo,endereco) values ('555444777-33','Miguel Xavier','M','Rua Bancário Sérgio Guerra,640');

-- inserindo dados nas colunas da tabela conta:
insert into conta (numero_conta,saldo,tipo_conta,num_agenda) values ('11765-2',222.20,2,'322');
insert into conta (numero_conta,saldo,tipo_conta,num_agenda) values ('21010-7',311.96,1,'1253');

-- inserindo dados nas colunas da tabela historico:
insert into historico (cpf_cliente,num_conta,data_inicio) values ('111222333-44','11765-2','2015-12-22'); 
insert into historico (cpf_cliente,num_conta,data_inicio) values ('666777888-99','11765-2','2016-10-05');
insert into historico (cpf_cliente,num_conta,data_inicio) values ('555444777-33','21010-7','2012-08-29');

-- inserindo dados nas colunas da tabela telefone cliente:
insert into telefone_cliente (cpf_cli,telefone) values ('111222333-44','833222-1234');
insert into telefone_cliente (cpf_cli,telefone) values ('666777888-99','839443-9999');
insert into telefone_cliente (cpf_cli,telefone) values ('666777888-99','833233-2267');

-- adicionando a coluna país na tabela cliente:
alter table cliente add column pais char(3) default ('BRA');
-- adicionando a coluna email na tabela cliente:
alter table cliente add column email varchar(30);
-- modificando o tipo da coluna numero_conta:
alter table conta modify numero_conta char(7);
-- adicionando um aumento de 10% no salario de uma das pessoas:
update conta set saldo = saldo*1.10 where tipo_conta = 2;
-- desativando o modo de atualização segura:
set sql_safe_updates = 0;
-- excluindo conta:
delete from agencia where numero_agencia = '322';
-- modificando o tipo de conta em saldos menores que 300:
update conta set tipo_conta = 3 where saldo < 300;


-- exibindo as tabelas para verificar se foram adicionados os valores:
select * from cliente;
select * from agencia;
select * from banco;
select * from conta;
select * from historico;
select * from telefone_cliente;
