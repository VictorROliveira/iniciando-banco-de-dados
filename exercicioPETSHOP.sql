create database bdPet;
use bdPet;

create table animal (
CodCliente int not null,
CodAnimal int not null,
Nome varchar(30),
Sexo char(1),
check (sexo in('M','F')),
Raca varchar(30),
Tipo varchar(30),
constraint pk_codanimal primary key (CodAnimal)
);

create table cliente (
CodCliente int not null,
Nome varchar(30) not null,
Telefone varchar(30),
Cpf char(12) not null,
unique(Cpf),
Endereco varchar(30),
constraint pk_codcliente primary key (CodCliente)
);

create table funcionario (
CodFuncionario int not null,
Tipo varchar(30),
Telefone varchar(11),
Nome varchar(30),
constraint pk_codfunc primary key (CodFuncionario)
);

create table loja (
CodLoja int not null,
Cnpj int not null,
Telefone varchar(11),
Endereco varchar(30),
constraint pk_codloja primary key (CodLoja)
);

create table vendas (
CodLoj int not null,
Preco decimal(7,2),
DataVenda date,
CodFunc int not null,
CodProduto int not null,
CodCli int not null,
CodVenda int not null,
constraint pk_codvenda primary key (CodVenda),
constraint fk_codloja foreign key (CodLoj) references loja (CodLoja),
constraint fk_codcliente foreign key (CodCli) references cliente (CodCliente), 
constraint fk_codfunc foreign key (CodFunc) references funcionario (CodFuncionario)
);

create table servico (
CodFun int not null,
Codcli int not null,
DataServ date,
Valor decimal(7,2),
Tipo varchar(30),
CodAni int not null,
CodServico int not null,
constraint pk_codserv primary key (CodServico),
constraint fk_codfun foreign key (CodFun) references funcionario (CodFuncionario),
constraint fk_codcl foreign key (Codcli) references cliente (CodCliente),
constraint fk_codanimal foreign key (CodAni) references animal (CodAnimal)
);

create table produto (
CodProduto int not null,
Marca varchar(30),
PesoLiquido decimal(6,2),
Tipo varchar(30),
ValorUnitario decimal(7,2),
constraint pk_codproduto primary key (CodProduto) 
);


