-- create database Aula1_p3;
-- use Aula1_p3;

create table cliente (
codigo int not null,
nome varchar(50) not null,
cpf char(12) not null,
endereco varchar(100),
telefone char(11),
unique (cpf),
primary key (codigo)
);

create table vendedor (
cv int not null,
nome varchar(50) not null,
salario decimal(7,2),
primary key (cv)
);

create table produto (
id int not null,
unidade char(3),
descricao varchar(50),
valor_unitario decimal(7,2),
primary key (id)
);

create table pedido (
numeroPedido int not null,
prazoEntrega date,
codigoCliente int not null,
codigoVendedor int not null,
primary key (numeroPedido, codigoCliente, codigoVendedor),
constraint chaveCliente foreign key (codigoCliente) references cliente(codigoC),
constraint chaveVendedor foreign key (codigoVendedor) references vendedor(codigo)
);

create table itemPedido (
numPedido int not null,
idProduto int not null,
quantidade int,
primary key (numPedido, idProduto),
constraint chavePedido foreign key (numPedido) references pedido(numeroPedido),
constraint chaveProduto foreign key (idProduto) references produto(id)
);
